package com.example.obvioustest.application

import android.app.Application
import android.content.Context
import com.example.obvioustest.data.data.NasaImageDataProvider
import com.example.obvioustest.presentation.repository.ImageRepository
import com.example.obvioustest.presentation.repository.ImageRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideContext(@ApplicationContext context: Context): Context {
        return context
    }

    @Singleton
    @Provides
    fun dataProvide(@ApplicationContext context: Context) = NasaImageDataProvider(context)

    @Provides
    @Singleton
    fun provideDataRepository(imageDataProvider: NasaImageDataProvider) =
        ImageRepositoryImpl(imageDataProvider) as ImageRepository


}