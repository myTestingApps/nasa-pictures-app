package com.example.obvioustest.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.example.obvioustest.R
import com.example.obvioustest.presentation.adapter.ImageClickCallback
import com.example.obvioustest.presentation.adapter.ImageListAdapter
import com.example.obvioustest.databinding.FragmentImageListBinding
import com.example.obvioustest.presentation.utils.Status
import com.example.obvioustest.presentation.viewmodel.ImageListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageListFragment : Fragment(), ImageClickCallback {

    lateinit var binding: FragmentImageListBinding

    private val viewModel: ImageListViewModel by activityViewModels()

    lateinit var listAdapter: ImageListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listAdapter = ImageListAdapter(this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentImageListBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initView()
        livedataListeners()
    }

    fun initView() {
        binding.rvImageList.adapter = listAdapter
    }

    private fun livedataListeners() {


        viewModel.imageApiResponseLiveData.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {
                    listAdapter.differ.submitList(response.data)

                }
                Status.ERROR -> {

                }
                Status.LOADING -> {

                }
            }

        })
    }

    override fun onImageClick(position: Int) {

        val bundle = Bundle()
        bundle.putInt("position", position)
        findNavController().navigate(
            R.id.action_imageListFragment_to_detailsFragment, bundle
        )
    }
}