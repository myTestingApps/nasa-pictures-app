package com.example.obvioustest.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.obvioustest.presentation.adapter.ImageDetailsAdapter
import com.example.obvioustest.databinding.FragmentDetailsBinding
import com.example.obvioustest.presentation.utils.Status
import com.example.obvioustest.presentation.utils.ZoomOutPageTransformer
import com.example.obvioustest.presentation.viewmodel.ImageDetailsViewModel
import com.example.obvioustest.presentation.viewmodel.ImageListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment : Fragment() {
    private val viewModel: ImageDetailsViewModel by viewModels()
    var clickedPosition = 0

    lateinit var binding: FragmentDetailsBinding


    private val detailsAdapter: ImageDetailsAdapter by lazy { ImageDetailsAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (requireArguments().containsKey("position")) {
            clickedPosition = requireArguments().getInt("position")
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.imageApiResponseLiveData.observe(viewLifecycleOwner, Observer { response ->

            when (response.status) {
                Status.SUCCESS -> {
                    detailsAdapter.differ.submitList(response.data)
                    binding.viewPager.adapter = detailsAdapter
                    binding.viewPager.setCurrentItem(clickedPosition, false)
                    binding.viewPager.setPageTransformer(ZoomOutPageTransformer())
                }
                Status.ERROR -> {

                }
                Status.LOADING -> {

                }
            }

        })

    }


}