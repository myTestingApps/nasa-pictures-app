package com.example.obvioustest.presentation.repository

import com.example.obvioustest.data.model.NasaDataResponseModelItem
import com.example.obvioustest.presentation.utils.ResourceUtil

interface ImageRepository {
    fun getImageData(): ResourceUtil<List<NasaDataResponseModelItem>>

}