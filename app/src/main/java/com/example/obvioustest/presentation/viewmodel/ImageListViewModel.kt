package com.example.obvioustest.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.obvioustest.R
import com.example.obvioustest.data.model.NasaDataResponseModel
import com.example.obvioustest.data.model.NasaDataResponseModelItem
import com.example.obvioustest.presentation.repository.ImageRepository
import com.example.obvioustest.presentation.utils.ResourceUtil
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject

@HiltViewModel
class ImageListViewModel @Inject constructor(
    private val imageRepository: ImageRepository,
    app: Application
) : AndroidViewModel(app) {


    private var _imageApiResponse: MutableLiveData<ResourceUtil<List<NasaDataResponseModelItem>>> =
        MutableLiveData()
    var imageApiResponseLiveData: LiveData<ResourceUtil<List<NasaDataResponseModelItem>>> =
        _imageApiResponse

    init {
        getImageList()
    }

    private fun getImageList() {
        viewModelScope.launch {
            _imageApiResponse.postValue(ResourceUtil.loading(null))
            _imageApiResponse.value = imageRepository.getImageData()
        }
    }
}