package com.example.obvioustest.presentation.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.obvioustest.R

class ImageViewBindings {
    companion object {

        @JvmStatic
        @BindingAdapter("nasa_image")
        fun nasaImage(view: ImageView, url: String?) {
            Glide.with(view.context).load(url)
                .placeholder(R.drawable.placeholder_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(view)

        }
    }
}