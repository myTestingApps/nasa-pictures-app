package com.example.obvioustest.presentation.adapter

interface ImageClickCallback {
    fun onImageClick(position:Int)
}