package com.example.obvioustest.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.obvioustest.data.model.NasaDataResponseModelItem
import com.example.obvioustest.databinding.ItemViewImageListBinding

class ImageListAdapter(private val clickCallback: ImageClickCallback) :
    RecyclerView.Adapter<ImageListAdapter.MyViewHolder>() {

    class MyViewHolder(val binding: ItemViewImageListBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        return MyViewHolder(
            ItemViewImageListBinding.inflate(
                LayoutInflater.from(parent.context), parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: NasaDataResponseModelItem = differ.currentList[position]
        holder.binding.itemInfo =item
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener {
            clickCallback.onImageClick(position)
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<NasaDataResponseModelItem>() {
        override fun areItemsTheSame(
            oldItem: NasaDataResponseModelItem,
            newItem: NasaDataResponseModelItem
        ): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(
            oldItem: NasaDataResponseModelItem,
            newItem: NasaDataResponseModelItem
        ): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, differCallback)


    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}