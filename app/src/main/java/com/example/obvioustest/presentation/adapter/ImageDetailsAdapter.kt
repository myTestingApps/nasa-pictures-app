package com.example.obvioustest.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.obvioustest.data.model.NasaDataResponseModelItem
import com.example.obvioustest.databinding.ItemViewImageDetailsBinding

class ImageDetailsAdapter :
    RecyclerView.Adapter<ImageDetailsAdapter.MyViewHolder>() {

    class MyViewHolder(val binding: ItemViewImageDetailsBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {


        return MyViewHolder(
            ItemViewImageDetailsBinding.inflate(
                LayoutInflater.from(parent.context), parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: NasaDataResponseModelItem = differ.currentList[position]
        holder.binding.itemInfo =item
        holder.binding.executePendingBindings()

    }

    private val differCallback = object : DiffUtil.ItemCallback<NasaDataResponseModelItem>() {
        override fun areItemsTheSame(
            oldItem: NasaDataResponseModelItem,
            newItem: NasaDataResponseModelItem
        ): Boolean {
            return oldItem.url == newItem.url
        }

        override fun areContentsTheSame(
            oldItem: NasaDataResponseModelItem,
            newItem: NasaDataResponseModelItem
        ): Boolean {
            return oldItem == newItem
        }
    }
    val differ = AsyncListDiffer(this, differCallback)


    override fun getItemCount(): Int {
        return differ.currentList.size
    }
}