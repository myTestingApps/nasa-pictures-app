package com.example.obvioustest.presentation.repository

import com.example.obvioustest.data.data.NasaImageDataProvider
import com.example.obvioustest.data.model.NasaDataResponseModel
import com.example.obvioustest.data.model.NasaDataResponseModelItem
import com.example.obvioustest.presentation.utils.ResourceUtil
import javax.inject.Inject

class ImageRepositoryImpl  @Inject constructor(
    private val nasaBusinessLogic: NasaImageDataProvider,

    ) : ImageRepository {
    override fun getImageData(): ResourceUtil<List<NasaDataResponseModelItem>> {
        return ResourceUtil.success(nasaBusinessLogic.invoke())
    }
}