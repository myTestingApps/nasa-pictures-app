package com.example.obvioustest.data.data

import android.content.Context
import com.example.obvioustest.R
import com.example.obvioustest.data.model.NasaDataResponseModel
import com.example.obvioustest.data.model.NasaDataResponseModelItem
import com.google.gson.Gson
import org.json.JSONArray
import java.nio.charset.Charset
import javax.inject.Inject

class NasaImageDataProvider @Inject constructor(private val context: Context) {

    operator fun invoke(): List<NasaDataResponseModelItem> {
        val inputStream = context.resources.openRawResource(R.raw.nasa_data)
        val imageDataArray = ArrayList<NasaDataResponseModelItem>()
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        val json = String(buffer, Charset.forName("UTF-8"))
        val items = JSONArray(json)
        val gson = Gson()
        for (i in 0 until items.length()) {
            val itemObject = items.getJSONObject(i)
            val info = gson.fromJson(itemObject.toString(), NasaDataResponseModelItem::class.java)
            imageDataArray.add(info)
        }
        return imageDataArray.sortedByDescending { it.date }
    }
}